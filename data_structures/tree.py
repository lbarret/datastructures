def connect(child, parent, idx=None, **kw):
    if parent in child.parents  : return
    if child in parent.children : return
    #==
    assert not parent in child.parents
    assert not child in parent.children
    #==
    child.pre_reparent_to(parent, **kw)
    parent.pre_add_child(child, **kw)
    #==
    child.parents.append(parent)
    if idx is None:
        parent.children.append(child)
    else:
        parent.children.insert(idx, child)
    #==
    child.post_reparent_to(parent, **kw)
    parent.post_add_child(child, **kw)

def disconnect(child, parent, **kw):
    if parent not in child.parents  : return
    if child not in parent.children : return
    #==
    assert parent in child.parents
    assert child in parent.children
    #==
    child.pre_deparent_from(parent, **kw)
    parent.pre_remove_child(child, **kw)
    #==
    child.parents.remove(parent)
    parent.children.remove(child)
    #==
    child.post_deparent_from( parent, **kw)
    parent.post_remove_child(child, **kw)

class Node(object):
    def __init__(self, cargo=None, children=None, is_root=False):
        self.children = []
        self.parents  = []
        self.cargo    = cargo
        self.is_root  = is_root
        #==
        if children:
            for c in children:
                connect(c, self)
    #==

    def reparent_to(self, parent, idx=None, **kw):
        connect(self, parent, idx=idx, **kw)

    def pre_reparent_to(self, parent, **kw):
        self.invariant()

    def post_reparent_to(self, parent, **kw):
        self.invariant()

    #==

    def deparent_from(self, parent, **kw):
        disconnect(self, parent, **kw)

    def pre_deparent_from(self, parent, **kw):
        self.invariant()

    def post_deparent_from(self, parent, **kw):
        self.invariant()

    #==

    def add_child(self, child, idx=None, **kw):
        connect(child, self, idx=idx, **kw)

    def pre_add_child(self, child, **kw):
        self.invariant()

    def post_add_child(self, child, **kw):
        self.invariant()

    #==

    def remove_child(self, child, **kw):
        disconnect(child, self, **kw)

    def pre_remove_child(self, child, **kw):
        self.invariant()

    def post_remove_child(self, child, **kw):
        self.invariant()

    #==

    def invariant(self):
        pass

    def __repr__(self):
        return repr(self.cargo) if self.cargo else str(self.__class__.__name__.lower())

class OneChildMixin(object):
    def invariant(self):
        assert len(self.children) <= 1

    def get_only_child(self):
        return self.children[0]

class OneParentMixin(object):
    def invariant(self):
        assert len(self.parents) <= 1

def walk_up_bf(nodes, acc=None):
    if not nodes:
        import pdb; pdb.set_trace()  # XXX BREAKPOINT
        return acc.reverse()
    #==
    parents = sum([node.parents[:] for node in nodes], [])
    acc.append(parents)
    return walk_up_bf(parents, acc)


def walk_up_df(node, acc, validator=None):
    acc.append(node)
    if not node.parents or node.is_root:
        acc.reverse()
        return [acc]
    meta_acc = []
    for p in node.parents:
        n_acc = acc[:]
        if validator and not validator(p):
            continue
        meta_acc += walk_up_df(p, n_acc, validator=validator)
    return meta_acc



def walk_up(node, acc=None, validator=None):
    #==
    if acc is None: acc = []
    if validator is None: validator = lambda x : True
    #==
    if not validator(node):
        acc.reverse()
        return [acc]
    #==
    acc.append(node)
    if node.parents == []:
        acc.reverse()
        return [acc]
    else:
        nacc = []
        for p in node.parents:
            nacc.extend( walk_up(p, acc[:], validator=validator) )
        return nacc


def dfs(node):
    visited  = set([])
    to_visit = [node]
    while to_visit:
        w = to_visit.pop()
        if w not in visited:
            yield w
            visited.add (w)
            to_visit.extend(w.children)

def map_ip(node, predicate, transform):
    for idx, c in enumerate(node.children[:]):
        map_ip(c, predicate, transform)
        if predicate(c):
            disconnect(c, node)
            connect(transform(c), node, idx=idx)

def prettyprint2(node):
    lvl     = 0
    strings = []
    nodes   = []
    seen    = set([])
    #==
    nodes.append((node, lvl))
    #==
    while nodes:
        #==
        node, lvl = nodes.pop()
        #==
        if node in seen:
            continue
        else:
            seen.add(node)
        #==
        _str = '  '*lvl+ str(node)+'\n'
        strings.append(_str)
        #==
        for child in reversed(node.children):
            nodes.insert(0, (child, lvl+1))
    return "".join(strings)

class SeenFilter(object):
    def __init__(self):
        self.seen = set([])

    def __call__(self, child):
        is_in = child in self.seen
        if not is_in:
            self.seen.add(child)
        return not is_in


def prettyprint(node, inc, _str=None, filter=None):
    filter = filter or SeenFilter()
    _str   = '  '*inc+ str(node)+'\n'
    for child in node.children:
        if not filter(child):
            continue
        _str += ''.join(prettyprint(child, inc+1, filter=filter))
    return _str

def reduce(node, func, joiner, filter=None, global_acc=None):
    filter = filter or SeenFilter()
    if not node.children:
        return func(node)
    else:
        local_acc = joiner.get_local_acc()
        for child in node.children:
            if not filter(child):
                continue
            else:
                res = reduce(child, joiner, filter=filter)
                local_acc.add(res)
        return local_acc


