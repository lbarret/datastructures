
from distutils.core import setup

setup(
    name             = 'DataStructures',
    version          = '0.1.0',
    author           = 'Lionel Barret',
    author_email     = 'lionel.barret@gmail.com',
    packages         = ['data_structures', 'data_structures.tests'],
    scripts          = [],
    url              = 'http://pypi.python.org/pypi/DataStructures/',
    license          = 'LICENSE.txt',
    description      = 'usual data structures',
    long_description = open('README.txt').read(),
    install_requires = [ ],
    )
